using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

 public class Vida : MonoBehaviour
{
    Animator Anim;
    public Image BarraVida;
    public Image BarraBullets;
    public float vidaActual;
    public float vidaMax;
    string varLingSalud;
    string varLingMuniciones;
    public float bulletsActual;
    public float bulletsMax;
    public Text Lvida,Mvida,Hvida;
    public Text Lbullet,Mbullet,Hbullet;
    ////
    int rutina;
    public int valorrutina;
    float cronometro;
    int direccion;
    public float speed_walk;
    public float speed_run;
    
    // Start is called before the first frame update
    void Start()
    {
        Anim=GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        BarraVida.fillAmount=vidaActual/vidaMax;
        BarraBullets.fillAmount=bulletsActual/bulletsMax;

        float pocoSalud = StartFunction(vidaActual,20f,35f);
        Lvida.text="L "+pocoSalud;
        float medioSalud = NormalFunction(vidaActual,20f, 40f, 60f, 80f);
        Mvida.text="M "+ medioSalud;
        float muchoSalud = EndFunction(vidaActual,63f, 81f);
        Hvida.text="H "+muchoSalud;


        float pocoBullets = StartFunction(bulletsActual,18f,30f);
        Lbullet.text="L "+pocoBullets;
        float medioBullets = NormalFunction(bulletsActual,16f,30f,45f,60f);
        Mbullet.text="M "+medioBullets;
        float muchoBullets = EndFunction(bulletsActual,45f,60f);
        Hbullet.text="H "+muchoBullets;

        varLingSalud = Analysis("LOW Health", "MID Health", "HIGH Health", pocoSalud, medioSalud, muchoSalud);
        varLingMuniciones = Analysis("LOW Bullets", "MID Bullets", "HIGH Bullets", pocoBullets, medioBullets, muchoBullets);

        rutina=valorrutina;
        LogicaEnemigo();
        Comportamientos();
    }

       
    public void OnSliderChanged(float Value)
    {
        vidaActual=Value;
    }
    public void OnSliderChanged2(float Value)
    {
        bulletsActual=Value;
    }
    void LogicaEnemigo()
    {
        if (varLingSalud == "LOW Health" &&  varLingMuniciones =="LOW Bullets")
        {
            rutina =0;    //ESCONDERSE
            //Anim.SetTrigger("Esconderse");
           //Console.WriteLine("ESCONDERSE");
        }
        else if (varLingSalud == "MID Health" &&  varLingMuniciones == "LOW Bullets")
        {
            rutina =0;   //ESCONDERSE
            //Anim.SetTrigger("Esconderse");
           //Console.WriteLine("ESCONDERSE");
        }
        else if (varLingSalud == "HIGH Health" &&  varLingMuniciones == "HIGH Bullets")
        {
            rutina=1;   //PATRULLAR
            //Anim.SetTrigger("Patrullar");
           //Console.WriteLine("PATRULLAR");
        }
        else if (varLingSalud == "MID Health" &&  varLingMuniciones == "HIGH Bullets")
        {
            rutina=1;  //PATRULLAR
            //Anim.SetTrigger("Patrullar");
           //Console.WriteLine("PATRULLAR");
        }
        else if (varLingSalud == "LOW Health" &&  varLingMuniciones == "MID Bullets")
        {
            rutina=2;   //HUIR
            //Anim.SetTrigger("Huir");
           //Console.WriteLine("HUIR");
        }
        else if (varLingSalud == "LOW Health" &&  varLingMuniciones =="HIGH Bullets")
        {
            rutina=2;  //HUIR
            //Anim.SetTrigger("Huir");
           //Console.WriteLine("HUIR");
        }
        else if (varLingSalud == "MID Health" &&  varLingMuniciones == "MID Bullets")
        {
            rutina=3;    //ATACAR
            //Anim.SetTrigger("Atacar");
           //Console.WriteLine("ATACAR");
        }
        else if (varLingSalud == "HIGH Health" &&  varLingMuniciones == "LOW Bullets")
        {
            rutina=3;   //ATACAR
            //Anim.SetTrigger("Atacar");
           //Console.WriteLine("ATACAR");
        }
        else if (varLingSalud == "HIGH Health" &&  varLingMuniciones == "MID Bullets")
        {
            rutina=3;    //ATACAR
            //Anim.SetTrigger("Atacar");
           //Console.WriteLine("ATACAR");
        }
    }


    //MEMBRESIA 
    
    // Función de inicio
    static float StartFunction(float x, float a, float b)
    {
        float membership = 0f;
        
        if (x <=a)
        membership = 1f;
        else if (x > a && x < b)
        membership = ((b-x)/(b-a));
        else if (x>= b)
        membership = 0f;
        
       return membership;
        
    }
    
    //Función del finalización
    
    static float EndFunction(float x, float a, float b)
    {
        float membership = 0f;
        
        if (x <= a)
        membership = 0f;
        else if (x>a && x<b)
        membership = ((x-a)/(b-a));
        else if (x >= b)
        membership = 1f;
        
        return membership;
    }
    
    // Función Normal
    
    static float NormalFunction(float x, float a, float b, float c, float d)
    {
        float membership = 0f;
        
        if (x <= a)
        membership = 0f;
        else if (x>a && x<b)
        membership = ((x-a)/(b-a));
        else if (x >= b && x <= c)
        membership = 1f;
        else if (x>c && x<d)
        membership = ((d-x)/(d-c));
        else if (x>=c)
        membership = 0f;
        
        return membership;
        
    }
    static string Analysis(string termLing1, string termLing2, string termLing3, float memb1, float memb2, float memb3)
    {
        //float eva1;
        //float eva2;
        //float eva3;
        string resultado = "";
        Dictionary<string,float> Eval = new Dictionary<string,float>();
        Eval.Add(termLing1,memb1);
        Eval.Add(termLing2,memb2);
        Eval.Add(termLing3,memb3);

        //eva1 = (OR(Eval[termLing1],Eval[termLing2]));
        //eva2 = (OR(Eval[termLing2],Eval[termLing3]));
        //eva3 = (OR(eva1,eva2));
        
        //foreach (var pair in Eval)
        //{
            //if(pair.Value == eva3)
            //{
                //resultado = pair.Key;
            //}
        //}
        return resultado;
    }
    public void Comportamientos()
    {
        cronometro += 1 * Time.deltaTime;
        if (cronometro >= 2)
        {
            direccion = Random.Range(0, 2);
            cronometro = 0;
        }
        switch(rutina)
        {
            case 0:
                Anim.SetTrigger("Esconderse");
                break;
            case 1:
                switch(direccion)
                {
                    case 0:
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        transform.Translate(Vector3.right * speed_walk * Time.deltaTime);
                        break;
                    case 1:
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        transform.Translate(Vector3.right * speed_walk * Time.deltaTime);
                        break;
                        
                }
                Anim.SetTrigger("Patrullar");
                break;
            case 2:
                switch(direccion)
                {
                    case 0:
                        transform.Translate(Vector3.right * speed_run * Time.deltaTime);
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        break;
                    case 1:
                        transform.Translate(Vector3.right * speed_run * Time.deltaTime);
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        break;
                        
                }
                Anim.SetTrigger("Huir");
                break;
            case 3:
                Anim.SetTrigger("Atacar");
                break;
        }

    }
}
